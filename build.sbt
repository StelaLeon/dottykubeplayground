enablePlugins(DockerComposePlugin)
enablePlugins(sbtdocker.DockerPlugin, JavaAppPackaging)
val dottyVersion = "0.19.0-RC1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "dotty-simple",
    version := "0.1.0",
    scalaVersion := dottyVersion,
    
    libraryDependencies ++= {
      val akkaVersion = "2.5.25"
      val akkaHttpVersion = "10.1.10"

      Seq(
        ("com.typesafe.akka" %% "akka-actor" % akkaVersion).withDottyCompat(scalaVersion.value) ,
        ("com.typesafe.akka" %% "akka-stream" % akkaVersion).withDottyCompat(scalaVersion.value),

        ("com.typesafe.akka" %% "akka-http" % akkaHttpVersion).withDottyCompat(scalaVersion.value)
      )
    },

    scalacOptions ++= {
      if (isDotty.value) Seq("-language:Scala2") else Nil
    },

    fork in run := true
  )

dockerfile in docker := {
  val appDir: File = stage.value
  val targetDir = "/app"

  new Dockerfile {
    from("openjdk:8-jre")
    entryPoint(s"$targetDir/bin/${executableScriptName.value}")
    copy(appDir, targetDir, chown = "daemon:daemon")
  }
}

imageNames in docker := Seq(
  ImageName(s"${organization.value}/${name.value}:latest"),
  ImageName(
    namespace = Some(organization.value),
    repository = name.value,
    tag = Some("v" + version.value)
  )
)

buildOptions in docker := BuildOptions(
  cache = false,
  removeIntermediateContainers = BuildOptions.Remove.Always,
  pullBaseImage = BuildOptions.Pull.Always
)

dockerImageCreationTask := docker.value

dockerExposedPorts := Seq(8080)
dockerExposedVolumes := Seq("/opt/docker/logs")

//dockerImageCreationTask := (publishLocal in Docker).value  //<<for local development

package dottyNewfeatures

//https://dotty.epfl.ch/docs/reference/new-types/intersection-types-spec.html

object IntersectionTypes {

  trait Resettable {
    def reset(): Unit
  }
  
  trait Growable[T]{
    def add(t:T): Unit
  }
  
  def f(x: Resettable & Growable[String]): Unit ={
    x.reset()
    x.add("first")
  }
  
  
  trait A{
    def as: List[A]
  }
  trait B{
    def as:List[B]
  }
  
  val x: A & B  = new C
  class C extends A with B{
    def as : List[A & B] = ???
  }
  
  val ys : List[A & B] = x.as
}

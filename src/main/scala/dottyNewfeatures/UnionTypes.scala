package dottyNewfeatures

object UnionTypes {

  type Hash = String
  case class UserName(name: String)
  case class Password(hash: Hash)
  
  def  help(id: UserName | Password) = 
    id match {
      case UserName(name) => name
      case Password(hash) => hash
  }
  
  val password = Password("123")
  val name = UserName("john")
  
  val v1: Password | UserName = if(true) name else password
  
 type LOGIN = [UserName, Password] =>> Map[UserName, Password]
  
  trait A { def hello: String }
  trait B { def hello: String }
  
  //The following code does not typecheck, because hello is not a member of AnyRef which is the join of A | B.
  //def test(x: A | B) = x.hello // error: value `hello` is not a member of A | B

}
